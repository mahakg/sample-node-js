Prerequisites to run this project:
Node.js installed in your machine
mongo db instance
node mongodb driver

I have made some changes in the dump file. There were duplicates entries for _id field for different sources. To avoid the use a scrpting file to import the data. I've deleted the lines having a  pattern "_id". I hope that's ok, as Mondodb will itself add a unique identifier for _id field.

To import the json dump in your mongo db please use below command in your terminal in the same directory where db file (Recruitment_backend_dump.txt) is present
mongoimport -db infoassembly --collection recruitment_new --jsonArray Recruitment_backend_dump.txt

execute js file to run it by http
node data_filters.js


Now please go to your browser and run localhost on port 8080
http://localhost:8080/

Everything show be running now.

You can filter data via source, Free text search on title and content


